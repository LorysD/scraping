FROM python:slim-buster
WORKDIR /tmp/NordPool-App

COPY . .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD python script.py

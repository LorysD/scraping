# Installation

Make sure to have docker and docker-compose installed on your machine.
Once you've cloned the repo, run the following command to start
and run the services :

```
docker-compose up -d
```

Docker compose launches two services. The first one will run selenium in a container.
This container will be viewed as the server. The second service, called client, uses
the selenium service to recover data from NordPool.

# Csv

Csvs strored in datasets are named using the last date of the data frame (first cell in the dataframe)

""" Creates csv and Parquet file from NordPools Market Data"""
import re
import time

from os import path
import schedule

from selenium import webdriver


import pandas as pd

# Get csv and parquet directories
DIR = path.dirname(__file__)
CSV_PATH = DIR + '/datasets'


def get_colnames(list_tag_colnames):
    """ Get list of column names
    from table at NordPool

    :param str list_tag_colnames: list of html tags containing columns among others
    :returns: list of columns for Market data table from NordPool
    :rtype: list

    """

    if not isinstance(list_tag_colnames, list):
        return ValueError("Input must be a list")

    # Remove elements from list that do not contain
    # the name of a column
    new_list = []

    for element in list_tag_colnames:
        if re.search('span', element) is not None:
            continue
        if re.search('<br>', element) is not None:
            offset = re.search('<br>', element).span()[1]
            element = element[offset:]
            new_list.append(element)


    # Remove duplicates
    new_list = list(dict.fromkeys(new_list).keys())
    new_list.insert(0, 'date')

    return new_list

def get_values(list_values):
    """Gets list of values from
    Market Data table that can be found at
    NordPool

    :param list_values: list values parsed from web page
    :returns: list table values from NordPool
    :rtype: list

    """

    if not isinstance(list_values, list):
        return ValueError("Input must be a list")

    lower, upper, index = 0, 1, 1
    rows = []
    list_length = len(list_values)

    # upper points to the first value of a row
    # (date) and upper points to the last one
    # When upper reaches another date we extracts
    # the row starting at lower and ending at upper-1

    while index < list_length:
        pattern = re.search('[0-9]{2}-[0-9]{2}-[0-9]{4}', list_values[index])
        if pattern is not None:
            row = list_values[lower:upper]
            row = [row[0]] + list(map(lambda x: re.split(',',x)[0] + '.' + re.split(',', x)[1],
                            row[1:]))
            rows.append(row)
            lower = upper
        index += 1
        upper = index
    return rows


def main_routine():
    """fetches data from nordpool
    and writes it to csv and parquet file

    :returns:
    :rtype:

    """
    options = webdriver.FirefoxOptions()
    options.add_argument('--ignore-ssl-errors=yes')
    options.add_argument('--ignore-certificate-errors')
    browser = webdriver.Remote(
            command_executor='http://host.docker.internal:4444/wd/hub',
            options = options
    )
    browser.get('https://www.nordpoolgroup.com/en/Market-data1/#/nordic/table')
    time.sleep(10)
    elements = browser.find_elements_by_tag_name('td')
    elements_list = list(map(lambda x: x.get_attribute('innerHTML'), elements))
    colnames = browser.find_elements_by_tag_name('th')
    colnames_elements = list(map(lambda x: x.get_attribute('innerHTML'), colnames))
    browser.quit()

    colnames = get_colnames(colnames_elements)
    rows = get_values(elements_list)

    data_frame = pd.DataFrame(rows, columns = colnames)
    last_date = data_frame['date'][0]
    # Writing csv
    data_frame.to_csv(CSV_PATH + f"/{last_date}.csv")
    return data_frame


if __name__ == '__main__':
    schedule.every().hour.do(main_routine)
    while True:
        schedule.run_pending()
